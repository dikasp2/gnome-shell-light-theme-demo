! main page: https://gitlab.gnome.org/dikasp2/gnome-shell-light-theme-experimentation

gnome shell (full) light theme suite
- dikasp @ gnome-design matrix
- dikasp2 @ gnome gitlab
- march 2023

theme installation:

1. extract and put contents under THEME folder into ~/.themes
2. install gnome shell extension manager & gnome tweaks
3. open gnome shell extension manager and enable user themes
4. open gnome tweaks
- select Default-light as shell themes
- select adw-gtk3 as legacy application themes
5. set wallpaper provided under WALLPAPER folder
6. thats it, i hope you enjoy it



optional extension
- rounded windows corners by yilozt (rounded window corner for legacy apps)
- dash to panel by jderose9 (win 10 panel style)
- floating panel by aylur (win 11 panel style/floating)
- just perfection by just-perfection (customize > panel position > bottom)

credits
- gnome shell classic light theme port
https://gitlab.gnome.org/eeshugerman/gnome-shell-theme-default-light
- libadwaita port for legacy applications
https://github.com/lassekongo83/adw-gtk3
- gnome 41 wallpaper
https://github.com/GNOME/gnome-backgrounds/tree/gnome-41

terimakasih! :)
